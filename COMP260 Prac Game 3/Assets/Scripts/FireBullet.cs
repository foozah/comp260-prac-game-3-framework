﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    private float bulletTime = 1.0f;
    public float reloadTime = 1.0f;
	// Use this for initialization
	void Start () {
		
	}

    public BulletMove bulletPrefab;
    void Update()
    {
        bulletTime += Time.deltaTime;
        // when the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1") && bulletTime >= reloadTime)
        {
            bulletTime = 0.0f;
            BulletMove bullet = Instantiate(bulletPrefab);
            // the bullet starts at the player's position
            bullet.transform.position = transform.position;
            // create a ray towards the mouse location
            Ray ray =
            Camera.main.ScreenPointToRay(Input.mousePosition);
            bullet.direction = ray.direction;
        }
    }
}
