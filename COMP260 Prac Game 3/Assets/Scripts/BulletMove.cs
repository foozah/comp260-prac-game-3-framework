﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;
    private float timer = 0.0f;
    public float timeLimit = 5.0f;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        timer += Time.deltaTime;
        rigidbody.velocity = speed * direction;
        if (timer >= timeLimit)
        {
            Destroy(gameObject);
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        // Destroy the bullet
        Destroy(gameObject);
    }
}
